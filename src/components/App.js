import React, { Component } from 'react';
import {  Container, Row, Col } from 'react-bootstrap';



//clientes apis
import socketIOClient from "socket.io-client";
import axios from 'axios';
import url from '../url_api';


//components
import Billetera from './Billetera';
import HeaderNav from './HeaderNav';
import UserVentaDivisa from './UserVentaDivisa';
import FormVenta from './FormVenta';
import GraficaTasaDivisa from './GraficaTasaDivisa';



class App extends Component {

    constructor(props){
        super();
    
        this.state = {

            user_id: localStorage.getItem('user_id'),
            tasa:{
              dolar:2400,
              euro: 2600
            },

            loading: true,

            venta:{
              userId:'',
              dolar: 0,
              euro: 0
            },

            item:{
              divisaTipo: '' ,
              cantidad : ''
            },

            status: false,

            userVentaDivisa:{},

            billetera:{
              dolar: 0,
              euro: 0 ,
              bolivar: 0
            }
        };

        //this.setFormVentaChange = this.setFormVentaChange.bind(this);

        this.setFormVenta = this.setFormVenta.bind(this);
        this.setCompraDivisa = this.setCompraDivisa.bind(this);

      }
    
      componentDidMount(){
        
        this.getTasaSocket();
        this.getBillertera();
        this.getUserVentaDivisa();
      }


      //servicios con la api funciones para consulta de data

      getTasaSocket = () => {  
        //consultamos en tiempo real el cambio en la tasa de divisa
        let socket = socketIOClient(`http://localhost:5000`);
        socket.on('test' , data => {
          let { DOLAR , EURO} = data;
          //se actuliza el estado de la tasa
          this.setState({ tasa:{ dolar: DOLAR , euro: EURO } , status: true , loading: false });
          
        });

      }

      //consulta a la api para optener la billetera del usuario logeado
      getBillertera = async () => {
        
        let user =  await axios(`${url}/user/${this.state.user_id}`);
       //actulizamos el estado en la billetera
        this.setState({billetera: user.data.billetera });
      }

      //consulta a la api para optener listado de usuario que venden divisa
      /*getUserVentaDivisa = async () => {
        let ventaData = await axios(`${url}/ventas`);
        
        //control de error si el listado esta vacio
        let ventas = ventaData.data.length > 0 ? ventaData.data: {};

        this.setState({ userVentaDivisa: ventas });
      }*/

      getUserVentaDivisa = () => {
        let socket = socketIOClient(`http://localhost:5000`);
        socket.on('venta' , ventaData => {
          let ventas = ventaData.length > 0 ? ventaData : {};
          console.log(ventas)
        this.setState({ userVentaDivisa: ventas });
          
        });
      }

      //funciones para actulizacion de la data en la api BD

      //funcion para el proce del formulario de venta de divisa
      setFormVenta = async (e) => {
        e.preventDefault();

        try {
            //optenemos los values de los inputs
            let divisaTipo = e.target[0].value; 
            let cantidad = e.target[1].value;

            //preparacion del dato 
            let venta = {
              userId: localStorage.getItem('user_id'),
              dolar: (divisaTipo === 'dolar' )? parseInt(cantidad) : 0,
              euro: (divisaTipo === 'euro' ) ? parseInt(cantidad) : 0
            }
            //consulta de la api usando async await
            await axios.post(`${url}/venta`,{venta});

            //manera basica para actulizar la billetera + el listado de usuario que venden divisa
            this.getBillertera();
           this.getUserVentaDivisa();
          
        } catch (error) {
          //error para developers
          console.log(error);
        }

      }

      //funcion para realizar la compra de divisa si es el caso
      setCompraDivisa = async (ventaId) => {

        try{
          //creamos la compra para enviar al servidor
          let compra = {
            ventaId: ventaId, 
            userIdComprador: this.state.user_id, 
            tasaCambio: this.state.tasa
          }
          //enviamos la data el servidor api
           await axios.post(`${url}/compra`,{compra});
           //actulizamos el estamo basico
          this.getBillertera();
          this.getUserVentaDivisa();

        }catch (error) { console.log(error)}

      }

      
    render(){
        //pasamos por props a los componentes hijos
        const { dolar , euro} =  this.state.tasa;

        return(
        <>
            <HeaderNav dolar={dolar} euro={euro}/>
            <Container>
            <Row className="top">
                <Col xs={9}>
                <UserVentaDivisa 
                 dolar={dolar} 
                 euro={euro}
                 status={this.state.status} 
                 userVentaDivisa={this.state.userVentaDivisa}
                 loading={this.loading}
                 setCompraDivisa={this.setCompraDivisa}
                 billetera={this.state.billetera}
                 user_id={this.state.user_id}/>

                  <GraficaTasaDivisa dolar={dolar} euro={euro}/>

                </Col>
                <Col xs={3}>
                  <Billetera billetera={this.state.billetera}/>

                  <FormVenta 
                  setFormVenta={this.setFormVenta}
                    item={this.state.item}
                    billetera={this.state.billetera}/>

                </Col>
            </Row>
            </Container>
        </>
        );
    }

}

export default App ;