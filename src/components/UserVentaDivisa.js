import React from 'react';
import { BarLoader } from 'react-spinners';

import { Card, Button, Table } from 'react-bootstrap';

const UserVentaDivisa = (props) => {

    const {bolivar} = props.billetera;
    return (
      <>
      <Card>
        <Card.Header className="black">
          Listado de divisa en ventas
          </Card.Header>
        <Card.Body>
          { props.userVentaDivisa.length > 0 ?
          <Table striped bordered hover size="sm" style={{textTransform:'uppercase'}}>
            <thead>
              <tr>
                <th>#</th>
                <th>USUARIO </th>
                <th>DIVISA</th>
                <th>CANT</th>
                <th>Bs</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
                 { props.userVentaDivisa.map((venta, i) => {

                    return (
                       venta.status === false ? 
                          <tr key={venta._id}>
                            <td width="1">{i+1}</td>
                            <td>{venta.userId.name.first} {venta.userId.name.last}</td>
                            <td>{venta.dolar > 0 ? 'Dolar' : 'Euro'}</td>
                            <td>{venta.dolar > 0 ? parseInt(venta.dolar) : parseInt(venta.euro)}</td>
                            <td>
                              {props.status === true ?
                                 new Intl.NumberFormat('de-DE').format( venta.dolar > 0 ? (parseInt(venta.dolar) * parseInt( props.dolar)) :
                                  (parseInt(venta.euro) * parseInt( props.euro)) ):
                                   <BarLoader sizeUnit={"px"} size={15} width={50} color={'#2d5456'} loading={props.loading}/> 
                              }
                            </td>
                            <td>
                            {venta.userId._id !== props.user_id ?  
                              
                                (bolivar > (parseInt(venta.dolar) * parseInt( props.dolar))) ? 
                                <Button block variant="primary" size="sm" 
                                onClick={props.setCompraDivisa.bind(this, venta._id)}
                                >Comprar</Button>
                                : <Button block variant="outline-warning" size="sm" disabled>SIN SALDO</Button>
                              
                              : <Button block variant="secondary" size="sm" disabled>COMPRAR</Button>
                             }
                            </td>
                          </tr>
                      :''
                    )
                  })
                 }
              </tbody>
            </Table>
          : 'No hay ventas disponibles...'}
        </Card.Body>
      </Card>
      </>
    )
}




export default UserVentaDivisa;


