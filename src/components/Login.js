import React, { Component } from 'react';
import {withRouter} from 'react-router-dom';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { Card, Form, Button, Container, Row, Col , Alert } from 'react-bootstrap';

import url  from '../url_api';

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            logueado: false,
            error: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    componentDidMount(){
        localStorage.clear();
        this.setState({logueado: false});
    }

    handleChange(event) {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({ [nam]: val });
    }

    handleSubmit= async (e) => {

        e.preventDefault();

        try {
            let user = await  axios.post(`${url}/login`, { email:this.state.email, password: this.state.password });
            localStorage.setItem('user_id', user.data[0].user[0]._id);
            this.setState({logueado: true});
            this.props.history.push('/home');

        } catch (error) {
            console.log(error);
            this.setState({error: true});
        }
       

    }


    render() {
        return (
            
            <Container>
             <Row>
                    <Col md={{ span: 4, offset: 4 }}>
                        <Card style={{ width: '100%', marginTop: '30px' }}>
                            <Card.Header className="black">
                                <h5 style={{ textAlign:'center', textTransform:'uppercase'}}>☭ PayBk</h5> 
                            </Card.Header>
                            <Card.Body>

                                {
                                    this.state.error !== false ? 
                                    <Alert variant="danger">
                                       Lo sentimos, ha ocurrido un error!
                                    </Alert> :  ''
                                }

                                <Form onSubmit={this.handleSubmit}>

                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Email </Form.Label>
                                        <Form.Control type="email" placeholder="Enter email" name="email" onChange={this.handleChange}/>
                                    </Form.Group>

                                    <Form.Group controlId="formBasicPassword">
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control type="password" placeholder="Password" name="password" onChange={this.handleChange}/>
                                    </Form.Group>

                                    <Button block  variant="outline-dark"  type="submit" size="sm" style={{marginRight:'5px'}}>
                                        Ingresar 
                                    </Button>
                                </Form>
                            </Card.Body>
                            <Card.Footer>
                                <Link to="/register" className="link" style={{marginTop:'10px', fontSize:'14px'}}>Crear una cuenta</Link>
                                </Card.Footer>
                        </Card>
                    </Col>
                </Row>
            </Container>

        );
    }
}



export default withRouter(Login);