import React from 'react';
//import axios from 'axios';
import { Card,Form, Button } from 'react-bootstrap';

//import URL from '../url_api';



const FormVenta = props => {

    const {divisaTipo , cantidad } = props;
            
 
    return (
      <Card style={{ width: '100%', marginTop: '10px' }}>
        <Card.Header className="black">
          Vender Divisa
          </Card.Header>
        <Card.Body>
          <Form onSubmit={props.setFormVenta}>

            <Form.Group controlId="exampleForm.ControlSelect1">
              <Form.Label>Divisa</Form.Label>
              <Form.Control as="select" value={divisaTipo} name="divisaTipo" >
                <option value="">Selecciona una divisa</option>
                <option value="dolar">Dolar</option>
                <option value="euro">Euro</option>
              </Form.Control>
            </Form.Group>

            <Form.Group controlId="exampleForm.ControlInput1">
              <Form.Label>Cantidad</Form.Label>
              <Form.Control type="number" placeholder="10" min="1" name="cantidad" value={cantidad} />
            </Form.Group>

            <Form.Group>
              
              <Button type="submit" size="sm" block  variant="outline-dark">VENDER</Button>
            </Form.Group>
          </Form>

        </Card.Body>
      </Card>
    );
}

export default FormVenta;


