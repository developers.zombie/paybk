import React from 'react';

import { Card } from 'react-bootstrap';



 const Billetera = props => {

    return (
          <>
            <Card style={{ width: '100%' }} >
              <Card.Header className="black">
                  Billetera
              </Card.Header>
              <Card.Body>
                <Card.Body>
                    <ul>
                      <li ><h6><b>Dolar</b>: {  props.billetera.dolar} </h6></li>
                      <li ><h6><b>Euro</b>: {  props.billetera.euro}</h6></li>
                      <li ><h6><b>Bolívar</b>: { new Intl.NumberFormat('de-DE').format(props.billetera.bolivar)} </h6></li>
                    </ul>
                </Card.Body>
              </Card.Body>
            </Card>
          </>
    );
  
}


export default Billetera;
