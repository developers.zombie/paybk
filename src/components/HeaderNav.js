import React , { Component } from 'react';

import axios from 'axios';
import { Link } from 'react-router-dom';
import { Navbar , Container ,Badge} from 'react-bootstrap';



class HeaderNav extends Component{

    constructor(props){
        super();

        this.state = {
            user:{
                name:''
            }
        };

    }

    componentDidMount() {
    
        this.getUserData().then(user =>{
            this.setState({user:{
                name: user.data.name,
               
            }});
        });
      }

    getUserData(){
        let user_id = localStorage.getItem('user_id');

        return axios('http://localhost:5000/api/user/' + user_id);
    }

    render() {

        const { user } = this.state;
      

        return (
            <Navbar bg="dark" variant="dark" className="b_button">
                <Container>
                    <Navbar.Brand href="#home"> ☭ PayBk</Navbar.Brand>
                    <Navbar.Toggle />

                    <Navbar.Collapse className="justify-content-center">
                        <Navbar.Text>
                           <Badge variant="success">
                               DOLAR USD : Bs { new Intl.NumberFormat('de-DE').format(this.props.dolar) }
                           </Badge>
                           <Badge variant="success" style={{ marginLeft: '10px'}}>
                              EURO : Bs { new Intl.NumberFormat('de-DE').format(this.props.euro) }
                           </Badge>
                        </Navbar.Text>
                    </Navbar.Collapse>
    
                    <Navbar.Collapse className="justify-content-end">
                        <Navbar.Text>
                          <Link to="/" className="link" style={{textTransform:'uppercase'}}>{user.name.first} {user.name.last}</Link>
                        </Navbar.Text>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        );
    }
}


export default HeaderNav;