import React from 'react';
import {Line} from 'react-chartjs-2';

import { Card } from 'react-bootstrap';

//ESTE COMPONTE ES DE PRUEBA
function GraficaTasaDivisa(props){



    const data = { 

     labels: [props.dolar , props.euro],
      datasets: [{
        label: "DOLAR",
        data: [ 0, props.dolar],
        lineTension: 0,
        fill: false,
        borderColor: 'orange',
        backgroundColor: 'transparent',
        borderDash: [5, 5],
        pointBorderColor: 'orange',
        pointBackgroundColor: 'rgb(132, 178, 105)',
        pointRadius: 5,
        pointHoverRadius: 10,
        pointHitRadius: 30,
        pointBorderWidth: 2,
        pointStyle: 'rectRounded'
      },
      {
        label: "EURO",
        data: [0, props.euro],
        lineTension: 0,
        fill: false,
        borderColor: 'orange',
        backgroundColor: 'transparent',
    
        pointBorderColor: 'black',
        pointBackgroundColor: 'rgba(255,150,0,0.5)',
        pointRadius: 5,
        pointHoverRadius: 10,
        pointHitRadius: 30,
        pointBorderWidth: 2,
        pointStyle: 'rectRounded'
      }
      ]
    };
    return (
        <Card style={{ width: '100%' ,  marginBottom:'20px', height:'360px', marginTop:'30px'}}>
          <Card.Header className="black">
            Gráfica USD - EURO
          </Card.Header>
          <Card.Body>
            <Line
              data={data}
              width={100}
              height={50}
              options={{ maintainAspectRatio: false }}
            />
          </Card.Body>
        </Card>
    );

}

export default GraficaTasaDivisa;