import React, { Component } from 'react';
import axios from 'axios';
import {withRouter , Link } from 'react-router-dom';
import { Card, Form , Container, Row, Col , Button , Alert} from 'react-bootstrap';

import url from '../url_api';



class Registro extends Component {

    constructor(props) {
        super(props);
        this.state = {
          
            first: '',
            last: '',
            
            email: '',
            password: '',

            error: false
          
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    componentDidMount() {
        localStorage.clear();
    }

    handleChange(event) {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({ [nam]: val });
    }

    handleSubmit = async (e) => {

        e.preventDefault();
        
        try {
          await axios.post(`${url}register`, 
            { 
                name: {
                first: this.state.first,
                last:  this.state.last
              },
            email:  this.state.email,
            password:  this.state.password
            }
         );
        
         this.props.history.push('/');

        } catch (error) {
            console.log(error);

            this.setState({error : true});
        }

       

    }


    render() {
        return (

            <Container>
                <Row>
                    <Col md={{ span: 4, offset: 4 }}>
                        <Card style={{ width: '100%', marginTop: '10px' }}>
                            <Card.Header className="black">
                               Nueva cuenta de usuario
                            </Card.Header>
                            <Card.Body>

                                {
                                    this.state.error !== false ? 
                                    <Alert variant="danger">
                                       Lo sentimos, ha ocurrido un error!
                                    </Alert> :  ''
                                }
                                <Form onSubmit={this.handleSubmit}>

                                <Form.Group controlId="formBasicfirst">
                                        <Form.Label>Nombre </Form.Label>
                                        <Form.Control type="text" placeholder="Nombre" name="first" onChange={this.handleChange} />
                                    </Form.Group>

                                    <Form.Group controlId="formBasiclast">
                                        <Form.Label>Apellido</Form.Label>
                                        <Form.Control type="text" placeholder="Apellido" name="last" onChange={this.handleChange} />
                                    </Form.Group>

                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Email </Form.Label>
                                        <Form.Control type="email" placeholder="Enter email" name="email" onChange={this.handleChange} />
                                    </Form.Group>

                                    <Form.Group controlId="formBasicPassword">
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control type="password" placeholder="Password" name="password" onChange={this.handleChange} />
                                    </Form.Group>

                                    <Button block  variant="outline-dark" type="submit" size="sm">
                                       Crear cuenta
                                    </Button>

                                </Form>

                            </Card.Body>

                            <Card.Footer>
                                <Link to="/" className="link" style={{marginTop:'10px', fontSize:'14px'}}>Ingresar a PayBk</Link>
                                </Card.Footer>
                        </Card>
                    </Col>
                </Row>
            </Container>

        );
    }
}



export default  withRouter(Registro);