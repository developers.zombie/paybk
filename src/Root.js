import React from 'react';
import {BrowserRouter as Router,Switch,Route} from "react-router-dom";


import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

//componentes
import Login from './components/Login';
import Registro from './components/Registro';
import App from './components/App';

function Root() {
  return (
    <>
    <Router>
        <Switch>
          <Route path="/home">
            <App/>
          </Route>
          <Route path="/register">
            <Registro/>
          </Route>
          <Route exact path="/">
            <Login/>
          </Route>
        </Switch>
    </Router>
    </>
  );
}

export default Root;


